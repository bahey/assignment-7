// By: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Arrays
// Index No: 19020082

#include <stdio.h>


//Addition
int addition(int row, int col)
{
	int mat1[row][col];
	int mat2[row][col];
	int result[row][col];
	int value, a, b, c, d, e,f;
	
	//taking input for Matrix One
	for (a=1; a<=row;a++)
	{
		for (b=1; b<=col; b++)
		{
			printf("Enter [%d][%d] of Matrix One : ", a, b);
			scanf("%d", &value);
			mat1[a][b] = value;
		}
	}
	puts("*****************************************");
	//taking input for Matrix Two
	for (c=1; c<=row;c++)
	{
		for (d=1; d<=col; d++)
		{
			printf("Enter [%d][%d] of Matrix Two : ", c, d);
			scanf("%d", &value);
			mat2[c][d] = value;
		}
	}
	
	//Calculating the Sum
	for (e=1;e<=row;e++)
	{
		for (f=1;f<=col;f++)
		{
			result[e][f] = mat1[e][f]+ mat2[e][f];
		}
	}
	//Printing the Sum
	int i,j;
	for (i=1; i<= row; i++)
	{
		for (j = 1; j<=col; j++)
		{
			printf("%d  \t", result[i][j]);
			if (j==col)
			{
				printf("\n\n\n");
			}
		}
	}
	
	printf("\n\n");
	return 0;
}//End of Addition

//Multiplication
int multiply(int row1,int col1,int row2,int col2 )
{
 int mat1[row1][col1];
 int mat2[row2][col2];
 int result[row1][col2];
 int value;
 int i,j,k,l;
 	for( i=1;i<=row1;i++)
		{
		for(k=1;k<=col1;k++)
			{
			printf("Enter [%d][%d] of Matrix One : " ,i,k);
			scanf("%d",&value);
			mat1[i][k]=value;
			}
		}
	puts("***************************************\n");
	for( j=1;j<=row2;j++)
		{
		for(l=1;l<=col2;l++)
			{
			printf("Enter [%d][%d] of Matrix Two : ",j,l);
			scanf("%d",&value);
			mat2[j][l]=value;
			}
		}
	
	
	for(i=1;i<=row1;i++)
		{
		for(j=1;j<=col2;j++)
			{
			result[i][j]=0;
			}
		}
		int r,s,x,y;
	
	// Multiplying Process
	for(r=1;r<=row1;r++)
		{
		for(s=1;s<=col2;s++)
			{
			for(x=1;x<=col1;x++)
				{
				result[r][s] += mat1[r][x]*mat2[x][s];
				}
			}
		}

	// Printing values
	printf("\n Multiply of two matrixes : \n");
	for(i =1; i<=row1;i++)
			{
			for(j =1;j<=col2;j++)
				{
				printf("%d ",result[i][j]);
				if(j == col2)
					{
					printf("\n\n");
					}
				}
			}
return 0;
	printf("\n\n");
}//End of Multiplication Function

//Main Function
int main()
{
	int row , col;
	int mat1[col][row];
	int mat2[col][row];
	int result[col][row];
	int value, a, b, c, d, e,f;
	
	
	int i;
printf("To ADD matrixes Enter '1' \tor\t Enter '0' to Multiply matrix : ");
scanf("%d",&i);
if(i == 1)
	{
		puts("You wanted to add two matrixes\n");
	printf("Enter the number of rows : ");
	scanf("%d",&row);
	printf("Enter the number of column : ");
	scanf("%d",&col);
	addition(row,col);
	}
if (i==0)
	{
		puts("You wanted to multiply two matrixes\n");
	int row1 , col1 , row2 , col2;
	puts("Enter the number of rows of first matrix : ");
	scanf("%d",&row1);
	puts("Enter the number of colomns of first matrix : ");
	scanf("%d",&col1);
	puts("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
	puts("Enter the number of rows of second matrix : ");
	scanf("%d",&row2);
	puts("Enter the number of columns of second matrix : ");
	scanf("%d",&col2);
	if(col1 == row2)
		{
		multiply(row1,col1,row2,col2);
		}
	else
		{
		printf("The Column number of first matrix must be equal to the row number of second matrix");
		}
	}
else{
		puts("You have entered a wrong input");
}
	
	
	return 0;
}

