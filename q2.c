// By: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Arrays
// Index No: 19020082

#include <stdio.h>

int main() {
    char string[1000];
    char charact;
    int i;
    int frequency = 0;
    
    // Getting the input string
    puts("Enter the string : ");
    gets(string);
    puts("Enter the character : ");
    charact = getchar();
    // Finding the frequency
    for (i = 0; i < strlen(string); i++) {
        if (charact == string[i]) {
            frequency++;
        }
    }
    printf("Frequency of \"%c\" is : %d", charact, frequency);
    return 0;
}
