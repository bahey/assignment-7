// By: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Arrays
// Index No: 19020082

#include <stdio.h>
int main()
{
	int n, x,y;
	char string[1000];
	
	//getting input sentence
	printf("Enter your sentence: \n");
	gets(string);
		
	//printing in reverse
	for (x = strlen(string)-1; x>=0; x--)
	{
		printf("%c",string[x]);
	}
	
	return 0;
}
